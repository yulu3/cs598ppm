for option in "" "--verbose" "--threads 228" "--threads 128" "--conflictList COLORING_ATOMIC" "--conflictList COLORING_PPS"
do
for data in circuit5M.mtx europe_osm.mtx hollywood-2009.mtx indochina-2004.mtx kron_g500-logn20.mtx Queen_4147.mtx
do
for alg in COLORING_EB COLORING_EBR COLORING_VBBIT
do
  export command="./cmake-build-debug/perf_test/graph/graph_color --cuda 0 --amtx /media/nbajiaoshi/Learn/Data/ParrellelGraphColoring/$data --algorithm $alg $option"
  echo $command
  $command
done done done